export const breakpoints = {
  mobile: 320,
  tablet: 768,
  desktop: 1024,
  xl: 1200,
};

export const colors = {
  pink: '#FE6675',
  yellow: '#FFCA03',
  white: '#fff',
  black: '#000',
};
