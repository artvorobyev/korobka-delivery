import React, { Component } from 'react';
import Header from '../../components/Header/Header';
import { PageContainer } from '../../components/PageContainer/PageContainer';
import { Heading } from '../../components/Heading/Heading';
import { PageLayout } from '../../components/PageLayout/PageLayout';

export default class OrderPage extends Component {
  render() {
    return (
      <PageLayout>
        <Header />
        <PageContainer color='yellow'>
          <Heading>I can deliver</Heading>
        </PageContainer>
      </PageLayout>
    );
  }
}
