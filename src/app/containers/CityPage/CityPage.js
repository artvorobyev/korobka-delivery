import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { CITIES } from '../../data/cities';
import Header from '../../components/Header/Header';
import Card from '../../components/Card/Card';
import { CityBody, CityBodyItem } from './styles';

class CityPage extends Component {
  checkForRedirect() {
    let { city } = this.props.match.params;
    if (!CITIES.filter((item) => city === item.route).length) {
      this.props.history.push('/');
    }
  }
  componentDidMount() {
    this.checkForRedirect();
  }
  render() {
    let { city } = this.props.match.params;
    return (
      <div>
        <Header></Header>
        <CityBody>
          <CityBodyItem>
            <Card
              link={`/${city}/order`}
              color='pink'
              title='I want to&nbsp;order'
              description='food, stuff, 
              medcines'
              icon={{
                x1: require('./images/order.png'),
                x2: require('./images/order@2x.png'),
              }}
            />
          </CityBodyItem>
          <CityBodyItem>
            <Card
              link={`/${city}/apply`}
              color='yellow'
              title='I can deliver'
              description='by car, bicycle, bike or on foot'
              icon={{
                x1: require('./images/bike.png'),
                x2: require('./images/bike@2x.png'),
              }}
            />
          </CityBodyItem>
        </CityBody>
      </div>
    );
  }
}

export default withRouter(CityPage);
