import styled from 'styled-components';
import { colors } from '../../styles/variables';
import { Link } from 'react-router-dom';

export const List = styled.ul`
  display: block;
  padding: 0;
  margin: 20px 0 0 0;
`;

export const ListItem = styled.li`
  display: block;
  padding: 0;
  margin: 0 0 10px 0;
`;

export const ListItemLink = styled(Link)`
  color: ${colors.white};
  text-decoration: none;
  font-size: larger;
`;

export const HomePageLayout = styled.div`
  padding: 20px;
`;
