import React, { Component } from 'react';
import './styles.js';
import { CITIES } from '../../data/cities';

import { Heading } from '../../components/Heading/Heading';
import { List, ListItem, ListItemLink, HomePageLayout } from './styles';

export default class HomePage extends Component {
  render() {
    return (
      <HomePageLayout>
        <Heading>Select a city:</Heading>
        <List>
          {CITIES.map((item, i) => {
            return (
              <ListItem key={i}>
                <ListItemLink to={`/${item.route}`}>{item.name}</ListItemLink>
              </ListItem>
            );
          })}
        </List>
      </HomePageLayout>
    );
  }
}
