import { Component } from 'react';
import { renderRoutes } from 'react-router-config';

export default class Root extends Component {
  render() {
    return renderRoutes(this.props.route.routes);
  }
}
