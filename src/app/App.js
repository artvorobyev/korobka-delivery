import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';

import Root from './containers/Root/Root';
import HomePage from './containers/HomePage/HomePage';
import CityPage from './containers/CityPage/CityPage';
import ApplyPage from './containers/ApplyPage/ApplyPage';
import OrderPage from './containers/OrderPage/OrderPage';

import { AppContainer } from './components/AppContainer/AppContainer';

const routes = [
  {
    component: Root,
    routes: [
      {
        path: '/',
        exact: true,
        component: HomePage,
      },
      {
        path: '/:city',
        component: CityPage,
        exact: true,
      },
      {
        path: '/:city/order',
        component: OrderPage,
        exact: true,
      },
      {
        path: '/:city/apply',
        component: ApplyPage,
        exact: true,
      },
    ],
  },
];

function App() {
  return (
    <AppContainer>
      <BrowserRouter>{renderRoutes(routes)}</BrowserRouter>
    </AppContainer>
  );
}

export default App;
