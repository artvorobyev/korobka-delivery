import styled from 'styled-components';
import { colors } from '../../styles/variables';

export const City = styled.div``;

export const Row = styled.div`
  font-size: 24px;
  line-height: 28px;
  color: ${colors.white};
  display: flex;
  justify-content: space-between;
  padding: 4px 5px 5px;
`;
