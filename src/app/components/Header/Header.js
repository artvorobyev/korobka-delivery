import React from 'react';
import { City, Row } from './styles';
import { CITIES } from '../../data/cities';
import { useParams } from 'react-router-dom';

const Header = (props) => {
  let { city } = useParams();
  let activeCity = CITIES.filter((item) => city === item.route)[0];
  return (
    <Row>
      <City>{activeCity.name}</City>
    </Row>
  );
};

export default Header;
