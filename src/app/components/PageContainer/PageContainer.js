import styled from 'styled-components';
import hexRgb from 'hex-rgb';
import { colors } from '../../styles/variables';

const boxShadowColor = (colorName) => {
  let hexColor = colors[colorName];
  let rgb = hexRgb(hexColor);
  return `${rgb.red},${rgb.green},${rgb.blue}`;
};

export const PageContainer = styled.div`
  background: ${(props) => colors[props.color]};
  box-shadow: 0px -2px 100px rgba(${(props) => boxShadowColor(props.color)}, 0.4);
  border-radius: 10px 10px 0px 0px;
  flex: 1;
  padding: 20px;
`;
