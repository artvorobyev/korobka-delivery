import styled from 'styled-components';
import { colors } from '../../styles/variables';

export const Heading = styled.div`
  color: ${colors.white};
  font-weight: 500;
  font-size: 48px;
  line-height: 48px;
`;
