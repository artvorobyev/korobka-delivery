import styled from 'styled-components';
import { colors } from '../../styles/variables';

export const CardContainer = styled.div`
  align-items: stretch;
  border-radius: 10px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-height: 310px;
  overflow: hidden;
  padding: 20px;
`;

export const CardContainerPink = styled(CardContainer)`
  background-color: ${colors.pink};
  box-shadow: 0px -2px 100px rgba(254, 102, 117, 0.4);
`;

export const CardContainerYellow = styled(CardContainer)`
  background-color: ${colors.yellow};
  box-shadow: 0px 2px 100px rgba(255, 202, 3, 0.3);
`;

export const CardTop = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const CardBottom = styled.div`
  align-items: flex-end;
  display: flex;
  justify-content: space-between;
`;

export const CardIcon = styled.img`
  height: 44px;
  margin-top: -10px;
  width: 44px;
`;

export const CardDescription = styled.div`
  color: ${colors.black};
  font-size: 24px;
  line-height: 28px;
  max-width: 172px;
`;
