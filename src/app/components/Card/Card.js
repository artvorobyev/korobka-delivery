import React, { Component } from 'react';
import { ArrowButton } from '../ArrowButton/ArrowButton';
import { Heading } from '../Heading/Heading';
import {
  CardContainerPink,
  CardContainerYellow,
  CardTop,
  CardBottom,
  CardIcon,
  CardDescription,
} from './styles';

const CardBody = (props) => {
  if (props.color === 'pink') {
    return <CardContainerPink>{props.children}</CardContainerPink>;
  } else {
    return <CardContainerYellow>{props.children}</CardContainerYellow>;
  }
};

export default class Card extends Component {
  componentDidMount() {}
  render() {
    return (
      <CardBody color={this.props.color}>
        <CardTop>
          <Heading>{this.props.title}</Heading>
          <CardIcon
            src={this.props.icon.x2}
            srcset={`${this.props.icon.x2} 2x`}
          />
        </CardTop>
        <CardBottom>
          <CardDescription>{this.props.description}</CardDescription>
          <ArrowButton to={this.props.link} />
        </CardBottom>
      </CardBody>
    );
  }
}
