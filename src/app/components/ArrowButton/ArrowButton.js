import styled from 'styled-components';
import { colors } from '../../styles/variables';
import { Link } from 'react-router-dom';

export const ArrowButton = styled(Link)`
  background-color: ${colors.white};
  background-image: url('${require('./images/arrow.svg')}');
  background-position: 50% 50%;
  background-repeat: no-repeat;
  border: none;
  border-radius: 50%;
  box-shadow: 0px 2px 50px rgba(0, 0, 0, 0.1);
  height: 100px;
  outline: none;
  width: 100px;

  &:active {
      opacity: 0.8;
  }
`;
